// First Step Imports
import { Fragment } from 'react';

import { Container } from 'react-bootstrap';

// Components
import CardHighlights from '../components/CardHighlights';
import SiteJumbotron from '../components/SiteJumbotron';
import Reviews from '../components/Reviews';
import Banner from '../components/Banner';
//import Featured from '../components/Featured';

export default function Home() {

	return (
		<Fragment>
			< SiteJumbotron />
			< Banner />
			<Container>
				< CardHighlights />
				{/*< Featured />*/}
				< Reviews />
			</Container>
		</Fragment>
	)
}