import { Link } from 'react-router-dom';

import { Jumbotron, Container, Button, Col, Row } from 'react-bootstrap';

export default function Error () {
	return (
	<Container className='padding-top-5'>
		<Jumbotron>
			<h1>Page Not Found</h1>
			<p>Browse our product categories or go to our <Link to="/products">products page</Link></p>
			<Row>
				<Col>
					<Button variant='primary'><Link to='/' className='text-white'>Product Category Here</Link></Button>
				</Col>
				<Col>
					<Button variant='primary'><Link to='/' className='text-white'>Product Category Here</Link></Button>
				</Col>
				<Col>
					<Button variant='primary'><Link to='/' className='text-white'>Product Category Here</Link></Button>
				</Col>
				<Col>
					<Button variant='primary'><Link to='/' className='text-white'>Product Category Here</Link></Button>
				</Col>
				<Col>
					<Button variant='primary'><Link to='/' className='text-white'>Product Category Here</Link></Button>
				</Col>
			</Row>
		</Jumbotron>
	</Container>
	)
}