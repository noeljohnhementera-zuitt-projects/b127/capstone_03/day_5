import { Card, Button, Container, Modal, Form } from 'react-bootstrap';

import { useState, useEffect, useContext, Fragment } from 'react';

import { useParams, Link } from 'react-router-dom';
// import useParams to untack the value of productId
import GlobalDataContext from '../GlobalDataContext';

import Swal from 'sweetalert2';

export default function SpecificProduct () {

	// unwrap react Context to know what the course the user has enrolled
	const { user } = useContext(GlobalDataContext); 

	// declare useParams() to untack productId
	const { productId } = useParams();

	// create useState() to store data
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ productName, setProductName ] = useState('')
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity] = useState(0);

	// purchase -> useState() for purchase modal button
	const [ showProductModal, setShowProductModal ] = useState(false);
	// openProductModal -> useState() to get the fetched data and become the setter or new value of the _id
	//const [ productId, setProductId ] = useState('');

	// purchase -> functions handling opening and closing of purchase modal
	const openProductModal = () =>{
		// Create a fetch for checking user's Order
		/*fetch(`http://localhost:4000/products/${ productId }`)
		.then(res => res.json())
		.then(data =>{
			// use setter in useState to change the update the properties of an object
			//setEachProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})*/
		// open the modal
		setShowProductModal(true)
	}

	// close the modal
	const closeProductModal = () => {
		setShowProductModal(false)
	}


	// fetch each data in each product
	useEffect(() =>{
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			setProductName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	const purchaseProduct = (e) =>{
		e.preventDefault();
		fetch('http://localhost:4000/users/order-successful', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data === false){
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: `Something went wrong. Please try again! `
				})
			}else{
				Swal.fire({
					title: 'Hooray!',
					icon: 'success',
					text: `Successfully purchased ${quantity} pieces of ${productName} worth Php ${price * quantity}`,
					footer: '<a href="http://localhost:3000/orders">Go to My Orders</a>'
				})
				closeProductModal();
			}
		})
	}

	return(
		<Fragment>
			<Container>
				<Card className='text-center padding-top-5'>
					<Card.Header className='bg-dark text-white'>
						<h3>{productName}</h3>
					</Card.Header>

					<Card.Body>
						<Card.Text>
							{description}
						</Card.Text>
						<h3>Php: {price}</h3>
					</Card.Body>

					<Card.Footer>
						{
							user.customerAccessToken !== null ?	
							<Fragment>
								<Button variant='primary' type='submit' block onClick={() => openProductModal()}>Select Quantity</Button>
								<Link className='btn btn-secondary btn-block' to='/products'>Go Back to Products</Link>
							</Fragment>
								:
								<Link className='btn btn-secondary btn-block' to='/login'>Login to Buy a Product</Link>
							
						}
					</Card.Footer>
				</Card>
			</Container>

		{/*Modal when purchasing a product*/}
			<Modal show={showProductModal} onHide={closeProductModal}>
				<Form onSubmit={(e) => purchaseProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Purchasing: {productName} worth &#8369;{price}!</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Quantity:</Form.Label>
							<Form.Control 
								type='number'
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Total:</Form.Label>
							<Form.Control 
								type='number'
								value={price * quantity}
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeProductModal}>Cancel</Button>
						<Button variant='success' type='submit'>Purchase</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</Fragment>
	)
}