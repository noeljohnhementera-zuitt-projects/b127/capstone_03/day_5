import { useState, useEffect, Fragment } from 'react';
import { Table, Button, Modal, Form, Container } from 'react-bootstrap';

import Swal from 'sweetalert2';
// Products = parent (passes data)
// AdminView = child (uses the data)
export default function CartView (props) {

	// use the data from parent component, Products
	const { productData, getProductsInCart } = props
	console.log(props)

	const [ products, setProducts ] = useState([])

	// addProduct -> create useState() to store data when adding a product
	const [ name, setName ] = useState('');
	const [ quantity, setQuantity ] = useState('');
	const [ totalAmount, setTotalAmount ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ productId, setProductId ] = useState('');

	// updateProductButton -> useState() for updateProductButton modal button
	const [ showEditProductModal, setShowEditProductModal ] = useState(false);
	// openEditModal -> useState() to get the fetched data and become the setter or new value of the _id
	
	// updateProductButton -> functions handling opening and closing of updateProductButton modal
	const openEditModal = (productId) =>{
		fetch(`http://localhost:4000/cart/${ productId }`)
		.then(res => res.json())
		.then(data =>{
			// use setter in useState to change the update the properties of an object
			setProductId(data._id)
			setPrice(data.price)
			setQuantity(data.quantity)
			setTotalAmount(data.totalAmount)
		})
		// open the modal
		setShowEditProductModal(true)
	}

	const closeEditModal = () =>{
		// close the modal
		setShowEditProductModal(false)
	}

	useEffect(()=>{
		const fetchProducts = productData.map(eachProduct =>{
			return (
			<tr	key={eachProduct._id} className='text-center'>
				<td>{eachProduct._id}</td>
				<td>{eachProduct.productId}</td>
				<td>{eachProduct.productName}</td>
				<td>{eachProduct.price}</td>
				<td>{eachProduct.quantity}</td>
				<td>{eachProduct.totalAmount}</td>
				<td>
					<Container className='d-flex justify-content-center'>
						{<Button variant='primary' size='sm' onClick={() => openEditModal(eachProduct._id)}>Edit Quantity</Button>}
					</Container>
				</td>
			</tr>
			)
		})

		setProducts(fetchProducts)
	}, [productData])

	// Edit a Product
	const editProductInCart = (e, productId) =>{
		e.preventDefault();
		fetch(`http://localhost:4000/cart/${productId}/updated`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
			},
			body: JSON.stringify({
				price: price,
				quantity: quantity,
				totalAmount: price * quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === false){
				getProductsInCart()
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: 'Please try again!'
				})	
			}else{
				getProductsInCart()
				Swal.fire({	
					title: "Success!",
					icon: 'success',
					text: 'Successfully updated!'
				})
				closeEditModal()
			}
		})
	}

	return (
		<Fragment>
			<div className='my-4 text-center'>
				<h2>Customer Cart</h2>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr className='text-center'>
						<th>ID</th>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total Amount</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		{/*Edit a Product Modal*/}
			<Modal show={showEditProductModal} onHide={closeEditModal}>
				<Form onSubmit={e => editProductInCart(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control 
								type='number'
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Quantity:</Form.Label>
							<Form.Control 
								type='number'
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Total:</Form.Label>
							<Form.Control 
								type='number'
								value={price * quantity}
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeEditModal}>Close</Button>
						<Button variant='success' type='submit'>Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</Fragment>
	)
}