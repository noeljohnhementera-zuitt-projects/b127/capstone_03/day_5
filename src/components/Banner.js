// Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner() {
	return (
	<Row>
		<Col>
			<Jumbotron className='text-center'>
				<h1>All Resto E-Commerce</h1>
				<p>Your Go-To Online Resto</p>
				<Row>
					<Col>
						<Link className='btn btn-primary mx-3' to={'/products'}>Browse our Products</Link>
						{/*<Button variant='primary' className='mx-3' href="#front">Featured Products</Button>*/}
					</Col>
				</Row>
			</Jumbotron>
		</Col>
	</Row>
		)
}