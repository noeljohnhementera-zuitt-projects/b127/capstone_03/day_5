import { Card, Col, Row, ListGroup, ListGroupItem, Button } from 'react-bootstrap';

import PropTypes from 'prop-types';

import { Link, useParams } from 'react-router-dom';

import { Fragment, useEffect, useState, useContext } from 'react';

//import Swal from 'sweetalert2';

//import GlobalDataContext from '../GlobalDataContext';

export default function OrderCard ( { productProp } ) {

	const { _id, customerId, productId, price, quantity, totalAmount, purchasedOn, productName } = productProp;

	const [ allProducts, setAllProducts ] = useState([])

	/*const { user } = useContext(GlobalDataContext);

	const { orderId } = useParams();

	const deleteOrder = (orderId) =>{
		fetch(`http://localhost:4000/orders/${orderId}/deleted`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('customerAccessToken')}`
			}
		})
			.then(res => res.json())
			.then(data =>{
				console.log(data)
				if(data.customerAccessToken !== undefined){
					Swal.fire({
						title: "Deleted Successfully!",
						icon: 'success',
						text: 'Order Transaction successfully deleted!'
					})
				}else{
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: 'Please try again!'
					})
				}
				setAllProducts(data)
			})
	}

	useEffect(()=>{
			deleteOrder()
		}, [])*/

	return (
		<Row>
			<Col className='padding-top-5 text-center'>
				<Card>
					<Card.Header><h3>Order Transaction</h3></Card.Header>
					<Card.Body>
						<Card.Title>Order ID:</Card.Title>
						<Card.Text key={_id}>{_id}</Card.Text>
						<Card.Title>Date Purchased:</Card.Title>
						<Card.Text>{purchasedOn}</Card.Text>
					</Card.Body>
					<ListGroup variant="flush">
						<ListGroup.Item><strong>Customer ID:</strong> {customerId}</ListGroup.Item>
						<ListGroup.Item><strong>Product ID:</strong> {productId}</ListGroup.Item>
						<ListGroup.Item><strong>Product Name:</strong> {productName}</ListGroup.Item>
						<ListGroup.Item><strong>Price:</strong> {price}</ListGroup.Item>
						<ListGroup.Item><strong>Quantity:</strong> {quantity}</ListGroup.Item>
						<ListGroup.Item><strong>Total Amount:</strong> {totalAmount}</ListGroup.Item>
					</ListGroup>
					{/*<Button variant='danger' onClick={() => deleteOrder(_id)}>Delete Order Transaction</Button>*/}
					<Link className='btn btn-primary' to={'/products'}>Browse More Products!</Link>
				</Card>
			</Col>
		</Row>
	)
}

OrderCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}