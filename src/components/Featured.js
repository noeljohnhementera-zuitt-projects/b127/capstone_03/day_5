import { Row, Col, Card, Button } from 'react-bootstrap';

import { Fragment } from 'react';

import { Link } from 'react-router-dom';

export default function Featured () {
	return (
		<Fragment>
			<h2 className='text-center padding-top-5'>Featured Products</h2>
		<Row>
			<Col md={12} lg={4} className='padding-top-5' id="front">
				<Card>
					<Card.Body>
						<Card.Title><h2>Cinnamon Baked French Toast</h2></Card.Title>
						<Card.Text>Ree Drummond's baked french toast is perfect for brunch or any special weekend breakfast.</Card.Text>
						<Card.Text><strong>Price:</strong> &#8369;99</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col md={12} lg={4} className='padding-top-5'>
				<Card>
					<Card.Body>
						<Card.Title><h2>Name</h2></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>	
						<Card.Text>Some Text Here</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col md={12} lg={4} className='padding-top-5'>
				<Card>
					<Card.Body>
						<Card.Title><h2>Name</h2></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>	
						<Card.Text>Some Text Here</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Fragment>
	)
}