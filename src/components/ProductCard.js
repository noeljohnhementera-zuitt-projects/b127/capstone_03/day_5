import { Row, Col, Card, Button, Modal, Form, Container } from 'react-bootstrap';

import PropTypes from 'prop-types';

import { Link, useParams } from 'react-router-dom';

import Swal from 'sweetalert2';

import { useState, Fragment, useEffect } from 'react';

export default function ProductCard ( { productProp } ) {

	const { _id, name, description, price } = productProp;

	const { productId } = useParams();

	const [ showProductModal, setShowProductModal ] = useState(false);
	const openProductModal = () => setShowProductModal(true)
	const closeProductModal = () => setShowProductModal(false)

	const [ productName, setProductName ] = useState('');
	const [ newPrice, setNewPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);

	const addProductToCart = (e) =>{
		e.preventDefault()
			fetch('http://localhost:4000/cart/added-cart', {
				method: 'POST',
			    headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('customerAccessToken')}`
				},
				body: JSON.stringify({
					productId: _id,
					productName: name,
					price: price,
					quantity: quantity
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data === false){
					Swal.fire({
							title: "Something went wrong!",
							icon: 'error',
							text: 'Please try again!'
						})
				}else{
					Swal.fire({
						title: 'Hooray!',
						icon: 'success',
						text: `Successfully purchased ${quantity} pieces of ${name} worth Php ${price * quantity}`,
						footer: '<a href="http://localhost:3000/cart">Go to Cart</a>'
					})
					closeProductModal();
				}
			})
	}

	return (
		<Fragment>
			<Row>
				<Col className='padding-top-5'>
					<Card>
						<Card.Body>
							<Card.Title><h2>{name}</h2></Card.Title>
							<Link className='btn btn-primary' to={`/products/${_id}`}>See Details</Link>
							<Button className='btn btn-primary' onClick={() => openProductModal()}>Add to Cart</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>

			{/*Modal when purchasing a product*/}
				<Modal show={showProductModal} onHide={closeProductModal}>
					<Form onSubmit={(e) => addProductToCart(e)}>
						<Modal.Header closeButton>
							<Modal.Title>Adding to Cart: {name} worth &#8369;{price}!</Modal.Title>
						</Modal.Header>

						<Modal.Body>

							<Form.Group>
								<Form.Label>Quantity:</Form.Label>
								<Form.Control 
									type='number'
									value={quantity}
									onChange={e => setQuantity(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Total:</Form.Label>
								<Form.Control 
									type='number'
									value={price * quantity}
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							<Button variant='secondary' onClick={closeProductModal}>Cancel</Button>
							<Button variant='success' type='submit'>Purchase</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		</Fragment>
	)
}

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}